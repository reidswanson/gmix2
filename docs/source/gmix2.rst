gmix2 package
=============

Submodules
----------

cgmix2 module
-------------------

.. automodule:: cgmix2
   :members:
   :undoc-members:
   :show-inheritance:

gmix2 module
------------------

.. automodule:: gmix2
   :members:
   :undoc-members:
   :show-inheritance:
